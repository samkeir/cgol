/**
  * FILE:          life.scala
  * AUTHOR:        Sam Keir
  *
  * DESCRIPTION:   An implementation of Conway's Game of Life in Scala.
  *
  * ARGUMENTS:     None.
  *
  * INPUT:         Board size, number of generations, number of initial living
  *                cells, locations of initial living cells.
  *
  * OUTPUT:        A printed board containing the display of the cells over the
  *                various generations.
  */


object life {

  val cell              = 'X'
  val syms              = " .+-|"
  val newline           = "\n"
  val titleMessage      = "\n**************************************\n" +
                          "****         Game of Life         ****\n" +
                          "**************************************\n\n"
  val genMessage1       = "\n====    GENERATION "
  val genMessage2       = "    ====\n"
  val enterBoardSize    = "Enter board size: "
  val enterGenNum       = "Enter number of generations to run: "
  val enterLiveCellsNum = "Enter number of live cells at start: "
  val enterStartLoc     = "Enter live cell locations:" +
                          " (Format: Row [ENTER] Col: [ENTER])\n"
  val errBoard          = "\nWARNING: Invalid board size. Enter board size: "
  val errGen            = "\nWARNING: Invalid number of generations. Enter" +
                          " number of generations to run: "
  val errCells          = "\nWARNING: Invalid number of live cells at" +
                          " start. Enter number of live cells at start: "
  val errLoc            = "\nWARNING: Invalid location. "


  /**
    * DESCRIPTION:  Performs the main execution of the game.
    * @param args   Program arguments, i.e. nothing.
    */
  def main(args:Array[String]): Unit ={
    print(titleMessage)
    initialize()
  }

  def initializeTest(): Unit ={

  }

  /**
    * DESCRIPTION:  Initializes the program and starts the game loop.
    */
  def initialize(): Unit ={
    val boardSize = initBoard()
    val genNum = initGenNum()
    val liveCells = initLiveCells()
    val board = initLoc(boardSize, liveCells)
    gameLoop(board, genNum, boardSize)
  }

  /**
    * DESCRIPTION:  Initializes the board.
    * @return       The size of the board.
    */
  def initBoard(): Int ={
    print(enterBoardSize)
    var boardSize = scala.io.StdIn.readInt()
    if(!(4 <= boardSize && boardSize <= 30)) {
      print(errBoard)
      boardSize = initBoard()
    }
    println()
    boardSize
  }

  /**
    * DESCRIPTION:  Initializes the number of generations.
    * @return       The number of generations.
    */
  def initGenNum(): Int ={
    print(enterGenNum)
    var genNum = scala.io.StdIn.readInt()
    if(!(0 <= genNum && genNum <= 20)) {
      print(errGen)
      genNum = initGenNum()
    }
    println()
    genNum
  }

  /**
    * DESCRIPTION:  Initializes the number of live cells at start.
    * @return       The number of live cells at start.
    */
  def initLiveCells(): Int ={
    print(enterLiveCellsNum)
    var liveCells = scala.io.StdIn.readInt()
    if(!(liveCells >= 0)) {
      print(errCells)
      liveCells = initLiveCells()
    }
    println()
    liveCells
  }

  /**
    * DESCRIPTION:      Initializes the starting locations of the live cells.
    * @param bSize      The board size.
    * @param liveCells  The number of live cells at the start.
    * @return           The board, now containing the starting live cells.
    */
  def initLoc(bSize : Int, liveCells : Int): Array[Array[Char]] ={
    print(enterStartLoc)
    var board = Array.ofDim[Char](bSize, bSize)
    for(x <- board.indices){
      for(y <- board(x).indices){
        board(x)(y) = '.'
      }
    }
    for(x <- 0 until liveCells){
      val row = scala.io.StdIn.readInt()
      val col = scala.io.StdIn.readInt()
      if(!validLoc(row, col, bSize, board)){
        print(errLoc)
        board = initLoc(bSize, liveCells)
      }
      setCell(row, col, cell, board)
    }
    println()
    board
  }

  /**
    * DESCRIPTION:      The main loop for the game.
    * @param board      The board containing the initial setup.
    * @param genNum     The number of generations to run.
    * @param boardSize  The size of the board.
    */
  def gameLoop(board : Array[Array[Char]], genNum : Int,
               boardSize : Int): Unit ={

    printGenHeader(0)
    printCells(board, boardSize)
    val trueBoard = Array.ofDim[Char](boardSize, boardSize)
    copyBoard(board, trueBoard)

    for(x <- 1 until genNum){

      val tempBoard = Array.ofDim[Char](boardSize, boardSize)
      copyBoard(trueBoard, tempBoard)

      for(x <- board.indices) {
        for (y <- board(x).indices) {
          val neighbors = countNeighbors(x, y, boardSize, trueBoard)
          if((neighbors <= 1) || (neighbors >= 4)) cellDie(x, y, tempBoard)
          else if(!cellAlive(x, y, trueBoard) && neighbors == 3)
            cellRevive(x, y, tempBoard)
        }
      }
      copyBoard(tempBoard, trueBoard)
      printGenHeader(x)
      printCells(trueBoard, boardSize)
    }

  }

  /**
    * DESCRIPTION:    Creates a deep copy of board1 into board2.
    * @param board1   The board to copy.
    * @param board2   The board to store the copy in.
    */
  def copyBoard(board1 : Array[Array[Char]], board2 : Array[Array[Char]]): Unit ={
    for(x <- board1.indices){
      for(y <- board1.indices){
        board2(x)(y) = board1(x)(y)
      }
    }
  }

  /**
    * DESCRIPTION:  Kills off the given cell.
    * @param row    The row of the given cell.
    * @param col    The column of the given cell.
    * @param board  The game board.
    */
  def cellDie(row : Int, col : Int, board : Array[Array[Char]]): Unit ={
    setCell(row, col, syms(1), board)
  }

  /**
    * DESCRIPTION:  Brings a given dead cell back to life.
    * @param row    The row of the given cell.
    * @param col    The column of the given cell.
    * @param board  The game board.
    */
  def cellRevive(row : Int, col : Int, board : Array[Array[Char]]): Unit ={
    setCell(row, col, cell, board)
  }

  /**
    * DESCRIPTION:      Checks if a given board location is valid.
    * @param row        The given row.
    * @param col        The given column.
    * @param boardSize  The board size.
    * @param board      The board.
    * @return           True if the location is valid, false otherwise.
    */
  def validLoc(row : Int, col : Int, boardSize : Int,
               board : Array[Array[Char]]) : Boolean ={
    var valid : Boolean = false
    if((0 <= row && row <= boardSize) && (0 <= col && col <= boardSize)){
      if(cellAlive(row, col, board)){
        return valid
      }
      valid = true
    }
    return valid
  }

  /**
    * DESCRIPTION:  Checks if the given cell is alive.
    * @param row    The given row.
    * @param col    The given column.
    * @param board  The board.
    * @return       True if the cell is alive, false otherwise.
    */
  def cellAlive(row : Int, col : Int, board : Array[Array[Char]]) : Boolean ={
    if(board(row)(col) == 'X') return true
    else return false
  }

  /**
    * DESCRIPTION:    Sets the given cell to the given value.
    * @param row      The given row.
    * @param col      The given column.
    * @param cellVal  The value to change the cell to.
    * @param board    The board.
    */
  def setCell(row : Int, col : Int, cellVal : Char,
              board : Array[Array[Char]]): Unit ={
    board(row)(col) = cellVal
  }

  /**
    * DESCRIPTION:        Counts the number of neighbors around the given cell.
    * @param row          The row of the given cell.
    * @param col          The column of the given cell.
    * @param boardSize    The size of the board.
    * @param board        The game board.
    * @return             The number of neighbors, as an Int.
    */
  def countNeighbors(row : Int, col : Int, boardSize : Int,
                     board : Array[Array[Char]]): Int ={
    var upperRow = row - 1
    var lowerRow = row + 1
    var leftCol = col - 1
    var rightCol = col + 1
    var neighbors = 0

    // Edge wrapping
    if(upperRow < 0) upperRow = boardSize - 1
    if(lowerRow >= boardSize) lowerRow = 0
    if(leftCol < 0) leftCol = boardSize - 1
    if(rightCol >= boardSize) rightCol = 0

    // top left
    if (cellAlive(upperRow, leftCol, board)) neighbors+=1
    // top center
    if (cellAlive(upperRow, col, board)) neighbors+=1
    // top right
    if (cellAlive(upperRow, rightCol, board)) neighbors+=1
    // middle left
    if (cellAlive(row, leftCol, board)) neighbors+=1
    // middle right
    if (cellAlive(row, rightCol, board)) neighbors+=1
    // bottom left
    if (cellAlive(lowerRow, leftCol, board)) neighbors+=1
    // bottom center
    if (cellAlive(lowerRow, col, board)) neighbors+=1
    // bottom right
    if (cellAlive(lowerRow, rightCol, board)) neighbors+=1

    neighbors
  }

  /**
    * DESCRIPTION:    Prints the header for the given generation number.
    * @param genNum   The given generation number.
    */
  def printGenHeader(genNum : Int): Unit ={
    print(genMessage1)
    print(genNum)
    print(genMessage2)
  }

  /**
    * DESCRIPTION:      Prints out the top/bottom border for the game board.
    * @param boardSize  The size of the board.
    */
  def printBorder(boardSize : Int): Unit ={
    print("          +")
    print("-" * boardSize)
    print("+\n")
  }

  /**
    * DESCRIPTION:      Prints out the bounding borders and the cells of the
    *                   game board.
    * @param board      The game board.
    * @param boardSize  The size of the board.
    */
  def printCells(board : Array[Array[Char]], boardSize : Int): Unit ={
    printBorder(boardSize)
    for(x <- board.indices){
      print("          |")
      for(y <- board.indices){
        print(board(x)(y))
      }
      print("|\n")
    }
    printBorder(boardSize)
  }
}
